#!/usr/bin/env python3

from setuptools import setup, find_packages
from distutils.util import convert_path

main_ns = {}
ver_path = convert_path('mud_engine/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

setup(
    name            = 'mud-engine',
    description     = 'A python based mud engine',
    author          = 'Ben Small',
    author_email    = 'benjamin.small83@gmail.com',
    url             = 'https://gitlab.com/benjamin.small83/mud-engine',
    python_requires = ">=3.7",
    install_requires = ['ansicolors'],
    packages        = find_packages(exclude=["tests", "docker"]),
    version         = main_ns['__version__'],
)
